<!doctype book public "-//OASIS//DTD DocBook V3.1//EN"
 [
<!ENTITY edition "9">
<!ENTITY intro SYSTEM "introduction.sgml">
<!ENTITY install SYSTEM "install.sgml">
<!ENTITY table SYSTEM "table.sgml">
<!ENTITY figure SYSTEM "figure.sgml">
<!ENTITY table.eg00 SYSTEM "table-eg00.sgml">
<!ENTITY table.eg01 SYSTEM "table-eg01.sgml">
<!ENTITY table.eg02 SYSTEM "table-eg02.sgml">
<!ENTITY table.eg1  SYSTEM "table-eg1.sgml">
<!ENTITY table.eg2  SYSTEM "table-eg2.sgml">
<!ENTITY table.eg3  SYSTEM "table-eg3.sgml">
<!ENTITY table.eg4  SYSTEM "table-eg4.sgml">
<!ENTITY table.land SYSTEM "table-land.sgml">
<!ENTITY table.small1 SYSTEM "table-small1.sgml">
<!ENTITY table.small2 SYSTEM "table-small2.sgml">
<!ENTITY table.small3 SYSTEM "table-small3.sgml">
<!ENTITY table.small4 SYSTEM "table-small4.sgml">
<!ENTITY equation.eg1 SYSTEM "equation-eg1.sgml">
<!ENTITY equation.eg2 SYSTEM "equation-eg2.sgml">
<!ENTITY equation.eg3 SYSTEM "equation-eg3.sgml">
<!ENTITY mathml SYSTEM "mathml.sgml">
<!ENTITY param SYSTEM "param.sgml">
<!ENTITY stycommand SYSTEM "stycommand.sgml">
<!ENTITY styoption SYSTEM "styoption.sgml">
<!ENTITY stypackage SYSTEM "stypackage.sgml">
<!ENTITY stydebug SYSTEM "stydebug.sgml">
<!ENTITY specparam SYSTEM "specparam.sgml">
<!ENTITY version SYSTEM "version.sgml">
<!ENTITY publish SYSTEM "publish.sgml">
<!ENTITY style SYSTEM "style.sgml">
<!ENTITY custom SYSTEM "custom.sgml">
<!ENTITY changes SYSTEM "changes-0.1.4.sgml">
<!ENTITY changes2 SYSTEM "changes-0.1.4.1.sgml">
<!ENTITY changes3 SYSTEM "changes-0.1.5.sgml">
<!ENTITY changes4 SYSTEM "changes-0.1.6.sgml">
<!ENTITY changes5 SYSTEM "changes-0.1.7.sgml">
<!ENTITY faq SYSTEM "faq.sgml">
]>

<book lang="en">
 <bookinfo>
  <title>
  DocBook to LaTeX Publishing 
 </title>
 <author>
  <firstname>Beno�t</firstname><surname>Guillon</surname>
 </author>
 <date>
  20 October 2005
 </date>
<subtitle>User Manual</subtitle>
<pubsnumber>Ref A0</pubsnumber>
<address>Toulon</address>
<edition>&edition;</edition>
<releaseinfo>Working Paper</releaseinfo>
<othercredit>
<firstname>Jean-Yves</firstname><surname>Le Ruyet</surname>
<contrib>REVIEWED BY</contrib>
</othercredit>
<othercredit>
<firstname></firstname><surname></surname>
<contrib>APPROVED BY</contrib>
</othercredit>
<revhistory>
<revision>
<revnumber>1</revnumber><date>20/01/03</date><revdescription>
 <para>
  First release of the package.
 </para>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>2</revnumber><date>30/04/03</date><revdescription>
 <para>
  Changes:
 </para>
 <itemizedlist>
  <listitem>
  <para>
  The script <filename>configure</filename> now checks the latex package dependencies, i.e. it checks that the packages used by the default DocBook latex style are available.
  </para>
 </listitem>
  <listitem>
  <para>
  The tool can be heavily customized thanks to a specification file and/or new extra options (cf. <xref linkend="sec-custom">).
  </para>
 </listitem>
 </itemizedlist>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>3</revnumber><date>11/06/03</date><revdescription>
 <para>
  Changes:
 </para>
 <itemizedlist>
  <listitem>
  <para>
  The <ulink url="http://xsltml.sourceforge.net">xsltml</ulink> library is included in the package to have a strong and consistent support of the MathML 2.0 specification.
  </para>
 </listitem>
  <listitem>
  <para>
  A large excerpt fo the MathML Test Suite 2.0 is now available to validate the MathML stylesheets.
  </para>
 </listitem>
 </itemizedlist>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision> 
<revision>
<revnumber>4</revnumber><date>03/07/03</date><revdescription>
 <para>
  Changes:
 </para>
 <itemizedlist>
  <listitem>
  <para>
  Dutch language is supported by the default latex stylesheets.
  </para>
 </listitem>
  <listitem>
  <para>
  The <sgmltag>subtitle</sgmltag> element is displayed on the cover page.
  </para>
 </listitem>
  <listitem>
  <para>
  Tables can be displayed in landscape, through the <sgmltag>orient</sgmltag> attribute. In addition, the table text size can be specified to be smaller by using the <sgmltag>role</sgmltag> attribute.
  </para>
 </listitem>
  <listitem>
  <para>
  Hyphenation is forced in tables, so that no words can cover several cells.
  </para>
 </listitem>
 </itemizedlist>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>5</revnumber><date>03/05/04</date>
<revdescription>
 <para>
  Changes: see <xref linkend="sec-changes-014">
 </para>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>6</revnumber><date>15/06/04</date>
<revdescription>
 <para>
  Changes: see <xref linkend="sec-changes-0141">
 </para>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>7</revnumber><date>15/07/05</date>
<revdescription>
 <para>
  Changes: see <xref linkend="sec-changes-015">
 </para>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>8</revnumber><date>25/09/05</date>
<revdescription>
 <para>
  Changes: see <xref linkend="sec-changes-016">
 </para>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
<revision>
<revnumber>&edition;</revnumber><date>20/10/05</date>
<revdescription>
 <para>
  Changes: see <xref linkend="sec-changes-017">
 </para>
</revdescription><authorinitials>B. Guillon</authorinitials>
</revision>
</revhistory>
</bookinfo>
<bibliography><title>Documentation</title>
<bibliodiv><title>Reference</title>
<biblioentry> 
<abbrev>TDG</abbrev> 
<title>DocBook: The Definitive Guide</title> 
<authorgroup><author><firstname>Norman</firstname><surname>Walsh</surname></author>
<author><firstname>Leonard</firstname><surname>Muellner</surname></author></authorgroup> 
<copyright><year>1999, 2000, 2001</year> <holder>O'Reilly &amp; Associates, Inc.</holder></copyright> 
<isbn>156592-580-7</isbn> 
<publisher><publishername>O'Reilly</publishername> </publisher> 
</biblioentry>
</bibliodiv>
</bibliography>
&intro;
&install;
 <chapter>
  <title>
  Using dblatex
 </title>
&publish;
&style;
&figure;
&table;
  <sect1>
   <title>
   Writing LaTeX mathematical equations
  </title>
   <sect2>
    <title>
    Presentation
   </title>
   <para>
    DocBook doesn't define elements for writing mathematical equations. Only few elements exist that tell how equation should be displayed (inlined, block):
   </para>
   <itemizedlist>
    <listitem>
    <para>
    <sgmltag>inlineequation</sgmltag> tells that the equation is inlined,
    </para>
   </listitem>
    <listitem>
    <para>
    <sgmltag>informalequation</sgmltag> tells that the equation is displayed as a block, without a title.
    </para>
   </listitem>
    <listitem>
    <para>
    <sgmltag>equation</sgmltag> tells that the equation is displayed as a block, with or without a title.
    </para>
   </listitem>
   </itemizedlist>
   <para>
    These tags include a graphic (<sgmltag>graphic</sgmltag> or <sgmltag>mediaobject</sgmltag>) or an alternative text equation, as shown by the example.
   </para>
<example><title>Equation taken from TDG</title>
   <programlisting>
<![CDATA[<equation><title>Last Theorem of Fermat</title> 
]]><![CDATA[  <alt>x^n + y^n &ne; z^n &forall; n &ne; 2</alt>
]]><![CDATA[  <graphic fileref="figures/fermat"></graphic>
]]><![CDATA[</equation>
]]>   </programlisting>
</example>
   </sect2>
   <sect2>
    <title>
    Implementation choice
   </title>
   <para>
    The principle is to use only the <sgmltag>alt</sgmltag> element. If initially <sgmltag>alt</sgmltag> contains actually the text to print, it is chosen to use this element to embed LaTeX mathematical equations. This choice has the following advantages:
   </para>
   <itemizedlist>
    <listitem>
    <para>
    The translation done by dblatex is really easy, since the equation is already written in LaTeX.
    </para>
   </listitem>
    <listitem>
    <para>
    LaTeX is one of the best word processor to render mathematical formulas.
    </para>
   </listitem>
    <listitem>
    <para>
    One doesn't need to write the equations in MathML.
    </para>
   </listitem>
    <listitem>
    <para>
    This method isn't specific to this tool (see the following section).
    </para>
   </listitem>
   </itemizedlist>
   <para>
    Besides, the implementation is as light as possible. This is why it is up to the writer to properly use the mathematical delimiters (&dollar;, &bsol;(, &bsol;), &bsol;&lsqb;, &bsol;&rsqb;). By this way the writer fully controls how he writes equations.
   </para>
   </sect2>
   <sect2>
    <title>
    Compatibility
   </title>
   <para>
    This implementation is not contradictory nor specific. In partticular, the <ulink url="http://ricardo.ecn.wfu.edu/~cottrell/dbtexmath/">DBTeXMath</ulink> proposal to extend the DSSSL stylesheets used by jade follows the same approach, and is integrated in the Norman Walsh XSL stylesheets.
   </para>
   </sect2>
   <sect2>
    <title>
    Examples
   </title>
   <para>
    The following examples show how to write the equations.
   </para>
<example><title>Inlined Equation</title>
&equation.eg1; 
<programlisting><textobject><textdata entityref="equation.eg1"></textobject>
</programlisting>
</example>
<example><title>Equation in a block</title>
&equation.eg2;
<programlisting><textobject><textdata entityref="equation.eg2"></textobject>
</programlisting>
</example>
<example><title>Equation in a float</title>
&equation.eg3;
<programlisting><textobject><textdata entityref="equation.eg3"></textobject>
</programlisting>
</example>
   </sect2>
  </sect1>
&mathml;
  <sect1>
   <title>
   Creating an Index
  </title>
  <para>
   An index is automatically generated if some index entries (<sgmltag>indexterm</sgmltag>), telling the terms to put in the index, are written in the document. The <sgmltag>keyword</sgmltag> elements are not printed but are also added to the index.
  </para>
<example><title>Index Entry</title>
  <programlisting>
<![CDATA[<para>In this paragraph is described the function 
]]><![CDATA[<function>strcpy</function><indexterm><primary>strcpy</primary></indexterm>.
]]><![CDATA[</para>
]]>  </programlisting>
</example>
  <para>
   The index is put at the end of the document. It is not possible to put it somewhere else.
  </para>
  </sect1>
  <sect1>
   <title>
   Writing a Bibliography
  </title>
  <para>
   A bibliography (<sgmltag>bibliography</sgmltag>) can be written and put anywhere in the document. It appears as a chapter or a section and is composed by several divisions (<sgmltag>bibliodiv</sgmltag>) displayed as sections or subsections.
  </para>
  <para>
   The writer selects information that describes each bibliography entry (<sgmltag>biblioentry</sgmltag>), and chooses the presentation order. The titles and authors are displayed first.
  </para>
<example><title>A Bibliography</title>
  <programlisting>
<![CDATA[<bibliography><title>Bibliography Example</title> 
]]><![CDATA[  <bibliodiv><title>References</title>
]]><![CDATA[    <biblioentry>
]]><![CDATA[      <title>Document title</title>
]]><![CDATA[      <author><firstname>J.</firstname><surname>Duval</surname></author>
]]><![CDATA[      <pubsnumber>DEX000567325</pubsnumber>
]]><![CDATA[    </biblioentry>
]]><![CDATA[  </bibliodiv>
]]><![CDATA[  <bibliodiv><title>White papers</title>
]]><![CDATA[    <biblioentry>
]]><![CDATA[      <title>Technical notes</title>
]]><![CDATA[      <authorgroup>
]]><![CDATA[        <author><firstname>J.</firstname><surname>Duval</surname></author>
]]><![CDATA[        <author><firstname>R.</firstname><surname>Marion</surname></author>
]]><![CDATA[      </authorgroup>
]]><![CDATA[      <pubsnumber>DEX000704520</pubsnumber>
]]><![CDATA[    </biblioentry>
]]><![CDATA[  </bibliodiv>
]]><![CDATA[</bibliography>
]]>  </programlisting>
</example>
  </sect1>
  <sect1>
   <title>
   Document Revisions
  </title>
  <para>
   The attribute <sgmltag>revisionflag</sgmltag> is usefull to identify the changes between two revisions of a document. This information is managed by dblatex, that adds revision bars in the margin of the paragraphs changed, such like in this <phrase revisionflag="changed">paragraph</phrase>.
  </para>
  <para>
   Adding the revision flags can be manual, but its is tedious and error prone. The perl script <ulink url="http://www.sun.com/xml/developers/diffmk/">diffmk</ulink> by Norman Walsh can do the work for you. It works fine, but it depends on several Perl modules.
  </para>
<note>
The revision bars only appear when using the "dvips" driver. It seems to be a limitation of the LaTeX macros defined by the changebar package.
</note>
  </sect1>
 </chapter>
 &custom;
 &faq;
 <chapter>
  <title>
  Thanks
 </title>
 <para>
  Thanks to the people who contributed to the project at its early age: Jean-Yves Le Ruyet, precursory and hard-working user, Julien Ducourthial for his precious help, Vincent Hottier who asked for the embedded LaTeX equations support.</para>
  <para>Special thanks to the KDE documentation team and Kai Brommann for their feedbacks, encouragements, and advice.
 </para>
 </chapter>

</book>
