<?php
$check_array = array();
$check_array[] = "kcontrol/";
$check_array[] = "kcontrol5/";
$check_array[] = "khelpcenter/"; #do we really need this?
$check_array[] = "kioworker/";
$check_array[] = "kioworker5/";
$check_array[] = "kioslave/";
$check_array[] = "kioslave5/";
/*
    paths which are meant to be ignored. Odd things 
    happening in the doc(s) dirs.
*/

$ignore_array = array();
$ignore_array[] = "applications/glossary";
$ignore_array[] = "kdelibs/common";
$ignore_array[] = "kdelibs/kdelibs";
$ignore_array[] = "kdelibs/api";
$ignore_array[] = "kdelibs/kjs";
$ignore_array[] = "kdelibs/kross";
$ignore_array[] = "kdelibs/kded4";
$ignore_array[] = "kdelibs/checkXML";
$ignore_array[] = "kdelibs/kdeoptions";
$ignore_array[] = "kdelibs/kioslave"; #needed to get no kioslave in Applications box
$ignore_array[] = "kdelibs/kjscmd";
$ignore_array[] = "kdelibs/kde4-config";
$ignore_array[] = "kdelibs/makekdewidgets";
$ignore_array[] = "kdelibs/kbuildsycoca4";
$ignore_array[] = "kdelibs/preparetips";
$ignore_array[] = "kdelibs/qtoptions";
$ignore_array[] = "kdelibs/meinproc4";
$ignore_array[] = "kdelibs/kconfig_compiler";
$ignore_array[] = "kdelibs/kdeinit4";
$ignore_array[] = "kdelibs/kcookiejar4";
$ignore_array[] = "kde-runtime/api";
$ignore_array[] = "kde-runtime/kioslave"; #needed to get no kioslave in Applications box
$ignore_array[] = "kde-runtime/glossary";
$ignore_array[] = "kde-runtime/nepomuk";
$ignore_array[] = "kde-workspace/plasma-desktop/tools";
$ignore_array[] = "kdegames/ksirk/NewDocSnapshots";
$ignore_array[] = "kdepim/akonadi_followupreminder_agent"; #temporarily ignore empty docs
$ignore_array[] = "kdepim/akonadi_notes_agent";
$ignore_array[] = "kdepim/contactthemeeditor";
$ignore_array[] = "kdepim/headerthemeeditor";
$ignore_array[] = "kdepim/sieveeditor"; # end ignore zone
$ignore_array[] = "kdepim/common";
$ignore_array[] = "kdepim/kioslave"; #needed to get no kioslave in Applications box
$ignore_array[] = "kdepim/api";
$ignore_array[] = "kdepimlibs/kioslave"; #needed to get no kioslave in Applications box
$ignore_array[] = "kdepimlibs/api";
$ignore_array[] = "kdevelop/api";
$ignore_array[] = "kdegames/api";
$ignore_array[] = "kdegames/ksirk/ksirkskineditor";
$ignore_array[] = "kdeedu/api";
$ignore_array[] = "kdeedu/kig/scripting-api";
$ignore_array[] = "kdevelop/tools";
$ignore_array[] = "kdemultimedia/kioslave"; #needed to get no kioslave in Applications box
$ignore_array[] = "kdesdk/scripts";
$ignore_array[] = "kdesdk/kmtrace";
$ignore_array[] = "kdesdk/poxml";
$ignore_array[] = "kdeutils/superkaramba";
$ignore_array[] = "kdeutils/superkaramba/api";
$ignore_array[] = "kdeutils/superkaramba/faq";
$ignore_array[] = "kdewebdev/kommander";
$ignore_array[] = "extragear-network/kioslave"; #needed to get no kioslave in Applications box
$ignore_array[] = "extragear-base/kappfinder";
$ignore_array[] = "extragear-graphics/digikam/project";
$ignore_array[] = "calligra/api";
$ignore_array[] = "calligra/krita";

$subdir_array= array();

$appdisplayname = array();
$appdisplayname_array["kcontrol"] = "System Settings Modules";
$appdisplayname_array["kcontrol5"] = "System Settings Modules";
$appdisplayname_array["kioworker"] = "KIO Workers";
$appdisplayname_array["kioworker5"] = "KIO Workers";
$appdisplayname_array["kioslave"] = "Kioslaves";
$appdisplayname_array["kioslave5"] = "Kioslaves";
#kdelibs
$appdisplayname_array["sonnet"] = "Check Spelling";
$appdisplayname_array["data"] = "Data URLs";
$appdisplayname_array["http"] = "http/https";
$appdisplayname_array["file"] = "file";
$appdisplayname_array["ftp"] = "FTP";
$appdisplayname_array["help"] = "help";
$appdisplayname_array["mailto"] = "mailto";
$appdisplayname_array["rlogin"] = "rlogin";
$appdisplayname_array["telnet"] = "telnet";
$appdisplayname_array["webdav"] = "webdav/webdavs";
#kdepimlibs
$appdisplayname_array["kresources"] = "KDE PIM Resources (deprecated)";
$appdisplayname_array["imap"] = "imap";
$appdisplayname_array["ldap"] = "ldap";
$appdisplayname_array["mbox"] = "mbox";
$appdisplayname_array["nntp"] = "nntp";
$appdisplayname_array["pop3"] = "pop3";
$appdisplayname_array["sieve"] = "sieve";
$appdisplayname_array["smtp"] = "smtp";
#applications
$appdisplayname_array["kdepasswd"] = "Password &amp; User Account"; # move to kcontrol, it is a kcm
$appdisplayname_array["kate"] = "Kate";
$appdisplayname_array["katepart"] = "Kate Part";
$appdisplayname_array["kfind"] = "KFind";
$appdisplayname_array["konsole"] = "Konsole";
$appdisplayname_array["konqueror"] = "Konqueror";
$appdisplayname_array["kwrite"] = "KWrite";
$appdisplayname_array["dolphin"] = "Dolphin";
$appdisplayname_array["keditbookmarks"] = "Bookmark Editor";
#kde-runtime
$appdisplayname_array["kdebugdialog"] = "KDebugDialog";
$appdisplayname_array["knetattach"] = "KNetAttach";
$appdisplayname_array["documentationnotfound"] = "Documentation not found";
$appdisplayname_array["onlinehelp"] = "Online Help";
$appdisplayname_array["khelpcenter"] = "KHelpCenter";
$appdisplayname_array["kdesu"] = "KDE su";
$appdisplayname_array["fundamentals"] = "KDE Fundamentals";
#kde-runtime kcontrol
$appdisplayname_array["attica"] = "Social Desktop";
$appdisplayname_array["bookmarks"] = "Bookmarks";
$appdisplayname_array["cache"] = "Cache";
$appdisplayname_array["componentchooser"] = "Default Applications";
$appdisplayname_array["cookies"] = "Cookies";
$appdisplayname_array["ebrowsing"] = "Web Shortcuts";
$appdisplayname_array["emoticons"] = "Emoticons";
$appdisplayname_array["filemanager"] = "File Management";
$appdisplayname_array["filetypes"] = "File Associations";
$appdisplayname_array["formats"] = "Formats";
$appdisplayname_array["history"] = "History";
$appdisplayname_array["icons"] = "Icons";
$appdisplayname_array["kcmcgi"] = "CGI Script";
$appdisplayname_array["kcmcss"] = "Appearance";
$appdisplayname_array["kcmlaunchfeedback"] = "Launch Feedback";
$appdisplayname_array["kcmnotify"] = "System Notification";
$appdisplayname_array["kcm_ssl"] = "SSL Preferences";
$appdisplayname_array["kded"] = "Service Manager";
$appdisplayname_array["khtml-adblock"] = "AdBlocK Filters";
$appdisplayname_array["khtml-behavior"] = "Web Browsing";
$appdisplayname_array["khtml-general"] = "General Konqueror Behavior";
$appdisplayname_array["khtml-java-js"] = "Java and JavaScript";
$appdisplayname_array["khtml-plugins"] = "Browser Plugins";
$appdisplayname_array["kwineffects"] = "Desktop Effects";
$appdisplayname_array["language"] = "Country/Region & Language";
$appdisplayname_array["nepomuk"] = "Desktop Search";
$appdisplayname_array["netpref"] = "Connection Preferences";
$appdisplayname_array["performance"] = "Performance";
$appdisplayname_array["phonon"] = "Audio and Video Settings";
$appdisplayname_array["proxy"] = "Proxy";
$appdisplayname_array["smb"] = "Windows Shares";
$appdisplayname_array["solid-device-automounter"] = "Removable Devices";
$appdisplayname_array["spellchecking"] = "Spell Checker";
$appdisplayname_array["translations"] = "Translations";
$appdisplayname_array["trash"] = "Trash";
$appdisplayname_array["useragent"] = "Browser Identification";
#kde-runtime kioslaves
$appdisplayname_array["bookmarks"] = "bookmarks";
$appdisplayname_array["bzip2"] = "bzip2 / bzip";
$appdisplayname_array["cgi"] = "cgi";
$appdisplayname_array["finger"] = "finger";
$appdisplayname_array["fish"] = "fish";
$appdisplayname_array["floppy"] = "Floppy";
$appdisplayname_array["gzip"] = "gzip";
$appdisplayname_array["info"] = "Info";
$appdisplayname_array["man"] = "Man";
$appdisplayname_array["nepomuksearch"] = "nepomuksearch";
$appdisplayname_array["network"] = "network";
$appdisplayname_array["nfs"] = "nfs";
$appdisplayname_array["sftp"] = "sftp";
$appdisplayname_array["smb"] = "SMB";
$appdisplayname_array["tar"] = "tar";
$appdisplayname_array["thumbnail"] = "thumbnail";
$appdisplayname_array["xz"] = "xz / lzma";
#kde-workspace
$appdisplayname_array["plasma-desktop"] = "Plasma Manual";
$appdisplayname_array["plasma-pa"] = "Audio Volume";
$appdisplayname_array["systemsettings"] = "System Settings";
$appdisplayname_array["kfontview"] = "Font Viewer";
$appdisplayname_array["klipper"] = "Klipper";
$appdisplayname_array["kdm"] = "KDM Login Manager";
$appdisplayname_array["kinfocenter"] = "KDE Info Center";
$appdisplayname_array["kmenuedit"] = "KDE Menu Editor";
$appdisplayname_array["ksysguard"] = "System Monitor";
$appdisplayname_array["PolicyKit-kde"] = "PolicyKit-kde";
#kde-workspace kcontrol
$appdisplayname_array["autostart"] = "Autostart";
$appdisplayname_array["baloo"] = "File Search Settings";
$appdisplayname_array["bell"] = "System Bell";
$appdisplayname_array["clock"] = "Date & Time";
$appdisplayname_array["colors"] = "Colors";
$appdisplayname_array["cursortheme"] = "Cursor Theme";
$appdisplayname_array["desktop"] = "Virtual Desktops";
$appdisplayname_array["desktopthemedetails"] = "Desktop Themes";
$appdisplayname_array["fontinst"] = "Font Management";
$appdisplayname_array["fonts"] = "Fonts";
$appdisplayname_array["joystick"] = "Joystick";
$appdisplayname_array["kcmaccess"] = "Accessibility";
$appdisplayname_array["kcmsmserver"] = "Session Management";
$appdisplayname_array["kcmstyle"] = "Style";
$appdisplayname_array["keyboard"] = "Keyboard";
$appdisplayname_array["keys"] = "Shortcuts";
$appdisplayname_array["kgamma5"] = "Monitor Gamma";
$appdisplayname_array["khotkeys"] = "Custom Shortcuts";
$appdisplayname_array["kwincompositing"] = "Desktop Effects";
$appdisplayname_array["kwindecoration"] = "Window Decorations";
$appdisplayname_array["kwinscreenedges"] = "Screen Edges";
$appdisplayname_array["kwintabbox"] = "Task Switcher";
$appdisplayname_array["mouse"] = "Mouse";
$appdisplayname_array["paths"] = "Paths";
$appdisplayname_array["powerdevil"] = "Power Management";
$appdisplayname_array["screenlocker"] = "Screen Locker";
$appdisplayname_array["screensaver"] = "Screen Locker";
$appdisplayname_array["solid-actions"] = "Device Actions";
$appdisplayname_array["solid-hardware"] = "Information Sources";
$appdisplayname_array["splashscreen"] = "Splash Screen";
$appdisplayname_array["webshortcuts"] = "Web Shortcuts";
$appdisplayname_array["windowbehaviour"] = "Window Behavior";
$appdisplayname_array["windowspecific"] = "Window Rules";
$appdisplayname_array["workspaceoptions"] = "Workspace";
#kdeaccessibility/
$appdisplayname_array["jovie"] = "Jovie";
$appdisplayname_array["kmag"] = "KMagnifier";
$appdisplayname_array["kmousetool"] = "KMouseTool";
$appdisplayname_array["kmouth"] = "KMouth";
#kdeadmin/
$appdisplayname_array["kcron"] = "Task Scheduler"; # move to kcontrol, it is a kcm
$appdisplayname_array["ksystemlog"] = "KSystemLog";
$appdisplayname_array["kuser"] = "KUser";
#kdeedu
$appdisplayname_array["artikulate"] = "Artikulate";
$appdisplayname_array["blinken"] = "Blinken";
$appdisplayname_array["cantor"] = "Cantor";
$appdisplayname_array["kalgebra"] = "KAlgebra";
$appdisplayname_array["kalzium"] = "Kalzium";
$appdisplayname_array["kanagram"] = "Kanagram";
$appdisplayname_array["kbruch"] = "KBruch";
$appdisplayname_array["kgeography"] = "KGeography";
$appdisplayname_array["khangman"] = "KHangMan";
$appdisplayname_array["kig"] = "Kig";
$appdisplayname_array["kiten"] = "Kiten";
$appdisplayname_array["klettres"] = "KLettres";
$appdisplayname_array["kmplot"] = "KmPlot";
$appdisplayname_array["kstars"] = "KStars";
$appdisplayname_array["ktouch"] = "KTouch";
$appdisplayname_array["kturtle"] = "KTurtle";
$appdisplayname_array["kwordquiz"] = "KWordQuiz";
$appdisplayname_array["marble"] = "Marble";
$appdisplayname_array["minuet"] = "Minuet";
$appdisplayname_array["pairseditor"] = "Pairs Editor";
$appdisplayname_array["parley"] = "Parley";
$appdisplayname_array["rocs"] = "Rocs";
$appdisplayname_array["step"] = "Step";
#kdegames
$appdisplayname_array["bomber"] = "Bomber";
$appdisplayname_array["bovo"] = "Bovo";
$appdisplayname_array["granatier"] = "Granatier";
$appdisplayname_array["kajongg"] = "Kajongg";
$appdisplayname_array["kapman"] = "Kapman";
$appdisplayname_array["katomic"] = "KAtomic";
$appdisplayname_array["knavalbattle"] = "Naval Battle";
$appdisplayname_array["kblackbox"] = "KBlackbox";
$appdisplayname_array["kblocks"] = "KBlocks";
$appdisplayname_array["kbounce"] = "KBounce";
$appdisplayname_array["kbreakout"] = "KBreakout";
$appdisplayname_array["kdiamond"] = "KDiamond";
$appdisplayname_array["kfourinline"] = "KFourInLine";
$appdisplayname_array["kgoldrunner"] = "KGoldrunner";
$appdisplayname_array["kigo"] = "Kigo";
$appdisplayname_array["killbots"] = "Killbots";
$appdisplayname_array["kiriki"] = "Kiriki";
$appdisplayname_array["kjumpingcube"] = "KJumpingCube";
$appdisplayname_array["klickety"] = "Klickety";
$appdisplayname_array["knights"] = "Knights";
$appdisplayname_array["klines"] = "Kolor Lines";
$appdisplayname_array["kmahjongg"] = "KMahjongg";
$appdisplayname_array["kmines"] = "KMines";
$appdisplayname_array["knetwalk"] = "KNetwalk";
$appdisplayname_array["kolf"] = "Kolf";
$appdisplayname_array["kollision"] = "Kollision";
$appdisplayname_array["konquest"] = "Konquest";
$appdisplayname_array["kpat"] = "KPatience";
$appdisplayname_array["kreversi"] = "KReversi";
$appdisplayname_array["kshisen"] = "Shisen-Sho";
$appdisplayname_array["ksirk"] = "KSirk";
$appdisplayname_array["kspaceduel"] = "KSpaceDuel";
$appdisplayname_array["ksquares"] = "KSquares";
$appdisplayname_array["ksudoku"] = "KSudoku";
$appdisplayname_array["ktron"] = "KSnakeDuel";
$appdisplayname_array["ktuberling"] = "KTuberling";
$appdisplayname_array["kubrick"] = "Kubrick";
$appdisplayname_array["lskat"] = "LSkat";
$appdisplayname_array["palapeli"] = "Palapeli";
$appdisplayname_array["picmi"] = "Picmi";
#kdegraphics
$appdisplayname_array["gwenview"] = "Gwenview";
$appdisplayname_array["kolourpaint"] = "KolourPaint";
$appdisplayname_array["kruler"] = "KRuler";
$appdisplayname_array["ksnapshot"] = "KSnapshot";
$appdisplayname_array["okular"] = "Okular";
$appdisplayname_array["spectacle"] = "Spectacle";
#kdegraphics kcontrol
$appdisplayname_array["kamera"] = "Digital Camera";
$appdisplayname_array["kgamma"] = "Monitor Gamma";
#kdemultimedia
$appdisplayname_array["dragonplayer"] = "Dragon Player";
$appdisplayname_array["elisa"] = "Elisa";
$appdisplayname_array["juk"] = "JuK";
$appdisplayname_array["kdenlive"] = "Kdenlive";
$appdisplayname_array["kmix"] = "KMix";
$appdisplayname_array["kwave"] = "Kwave";
#kdemultimedia kcontrol
$appdisplayname_array["cddbretrieval"] = "CDDB Retrieval";
$appdisplayname_array["cddbretrieval5"] = "CDDB Retrieval";
$appdisplayname_array["kcmaudiocd"] = "Audiocd IO Slave Configuration";
#kdemultimedia kioslave
$appdisplayname_array["audiocd"] = "audiocd";
#kdenetwork
$appdisplayname_array["kget"] = "KGet";
$appdisplayname_array["kopete"] = "Kopete";
$appdisplayname_array["kppp"] = "KPPP";
$appdisplayname_array["krdc"] = "KRDC";
$appdisplayname_array["krfb"] = "Desktop Sharing";
$appdisplayname_array["recentdocuments"] = "Recent Documents";
#kdepim
$appdisplayname_array["akonadi_archivemail_agent"] = "Archive Mail Agent";
$appdisplayname_array["akonadi_followupreminder_agent"] = "Follow Up Reminder Agent";
$appdisplayname_array["akonadi_folderarchive_agent"] = "Folder Archive Agent";
$appdisplayname_array["akonadi_notes_agent"] = "Notes Agent";
$appdisplayname_array["akonadi_sendlater_agent"] = "Send Later Agent";
$appdisplayname_array["akregator"] = "Akregator";
$appdisplayname_array["blogilo"] = "Blogilo";
$appdisplayname_array["contactthemeeditor"] = "Contact Theme Editor";
$appdisplayname_array["headerthemeeditor"] = "Header Theme Editor";
$appdisplayname_array["importwizard"] = "Import Wizard";
$appdisplayname_array["kabcclient"] = "KABC-Client";
$appdisplayname_array["kaddressbook"] = "KAddressBook";
$appdisplayname_array["kalarm"] = "KAlarm";
$appdisplayname_array["kjots"] = "KJots";
$appdisplayname_array["kleopatra"] = "Kleopatra";
$appdisplayname_array["kmail"] = "KMail";
$appdisplayname_array["kmail2"] = "KMail";
$appdisplayname_array["kmailcvt"] = "KMailCVT";
$appdisplayname_array["knode"] = "KNode";
$appdisplayname_array["knotes"] = "KNotes";
$appdisplayname_array["konsolekalendar"] = "KonsoleKalendar";
$appdisplayname_array["kontact"] = "Kontact";
$appdisplayname_array["kontact-admin"] = "Kontact Administrator's Guide";
$appdisplayname_array["korganizer"] = "KOrganizer";
$appdisplayname_array["ktnef"] = "KTnef";
$appdisplayname_array["kwatchgnupg"] = "KWatchGnuPG";
$appdisplayname_array["pimsettingexporter"] = "PIM Setting Exporter";
$appdisplayname_array["sieveeditor"] = "Sieve Script Editor";
#kdepim kioslave
$appdisplayname_array["news"] = "news";
#kdesdk
$appdisplayname_array["cervisia"] = "Cervisia";
$appdisplayname_array["kapptemplate"] = "KAppTemplate";
$appdisplayname_array["kcachegrind"] = "KCachegrind";
$appdisplayname_array["kompare"] = "Kompare";
$appdisplayname_array["lokalize"] = "Lokalize";
$appdisplayname_array["okteta"] = "Okteta";
$appdisplayname_array["umbrello"] = "Umbrello";
#kdetoys
$appdisplayname_array["amor"] = "Amor";
$appdisplayname_array["kteatime"] = "KTeatime";
#kdeutils
$appdisplayname_array["ark"] = "Ark";
$appdisplayname_array["filelight"] = "Filelight";
$appdisplayname_array["kbackup"] = "KBackup";
$appdisplayname_array["kcalc"] = "KCalc";
$appdisplayname_array["kcharselect"] = "KCharSelect";
$appdisplayname_array["kdf"] = "KDiskFree";
$appdisplayname_array["kfloppy"] = "KFloppy";
$appdisplayname_array["kgpg"] = "KGpg";
$appdisplayname_array["ktimer"] = "KTimer";
$appdisplayname_array["kwallet"] = "KWallet";
$appdisplayname_array["kwallet5"] = "KWallet";
$appdisplayname_array["kwalletmanager"] = "KWallet Manager";
$appdisplayname_array["sweeper"] = "Sweeper";
#kdeutils kcontrol
$appdisplayname_array["blockdevices"] = "Block Devices";
$appdisplayname_array["kremotecontrol"] = "Remote Control";
#kdewebdev
$appdisplayname_array["kfilereplace"] = "KFileReplace";
$appdisplayname_array["kimagemapeditor"] = "KImageMapEditor";
$appdisplayname_array["klinkstatus"] = "KLinkStatus";
#calligra
$appdisplayname_array["sheets"] = "Calligra Sheets";
$appdisplayname_array["calligra"] = "Calligra";
$appdisplayname_array["calligraplan"] = "Calligra Plan";
$appdisplayname_array["kexi"] = "Kexi";
$appdisplayname_array["stage"] = "Calligra Stage";
#extragear-accessibility
$appdisplayname_array["afaras"] = "Afaras";
$appdisplayname_array["ssc"] = "Sample Acquisition Tool";
$appdisplayname_array["sscd"] = "Sample Acquisition Tool Daemon";
$appdisplayname_array["simond"] = "Simon Daemon";
$appdisplayname_array["simon"] = "Simon Speech Recognition Tool";
$appdisplayname_array["sam"] = "Simon Accoustic Model Manager";
$appdisplayname_array["ksimond"] = "Simon Daemon Frontend";
#extragear-base kcontrol
$appdisplayname_array["wacomtablet"] = "KCM Tablet";
#extragear-edu
$appdisplayname_array["gcompris"] = "GCompris Administration";
$appdisplayname_array["labplot2"] = "LabPlot";
#extragear-games
#extragear-graphics
$appdisplayname_array["digikam"] = "digiKam";
$appdisplayname_array["kcoloredit"] = "KColorEdit";
$appdisplayname_array["kgraphviewer"] = "KGraphViewer";
$appdisplayname_array["kiconedit"] = "KIconEdit";
$appdisplayname_array["kipi-plugins"] = "Kipi Plugins";
$appdisplayname_array["kphotoalbum"] = "KPhotoAlbum";
$appdisplayname_array["kpovmodeler"] = "KPovModeler";
$appdisplayname_array["kuickshow"] = "KuickShow";
$appdisplayname_array["kxstitch"] = "KXStitch";
$appdisplayname_array["showfoto"] = "Showfoto";
$appdisplayname_array["skanlite"] = "Skanlite";
$appdisplayname_array["SymbolEditor"] = "SymbolEditor";
#extragear-kdevelop
$appdisplayname_array["kdevelop"] = "KDevelop";
#extragear-multimedia
$appdisplayname_array["amarok"] = "Amarok";
$appdisplayname_array["k3b"] = "K3b";
$appdisplayname_array["kaffeine"] = "Kaffeine";
$appdisplayname_array["kamoso"] = "Kamoso";
$appdisplayname_array["kaudiocreator"] = "KAudioCreator";
$appdisplayname_array["kmid"] = "KMid";
$appdisplayname_array["kmplayer"] = "KMPlayer";
$appdisplayname_array["kplayer"] = "KPlayer";
#extragear-network
$appdisplayname_array["choqok"] = "Choqok";
$appdisplayname_array["kopete-cryptography"] = "Cryptography Plugin";
$appdisplayname_array["rekonq"] = "rekonq";
$appdisplayname_array["konversation"] = "Konversation";
$appdisplayname_array["ktorrent"] = "KTorrent";
$appdisplayname_array["smb4k"] = "Smb4K";
#extragear-network kioslave
$appdisplayname_array["gopher"] = "gopher";
$appdisplayname_array["gdrive"] = "gdrive";
#extragear-office
$appdisplayname_array["kbibtex"] = "KBibTeX";
$appdisplayname_array["kile"] = "Kile";
$appdisplayname_array["kmymoney"] = "KMyMoney";
$appdisplayname_array["skrooge"] = "Skrooge";
$appdisplayname_array["tellico"] = "Tellico";
#extragear-pim
$appdisplayname_array["ksig"] = "KSig";
$appdisplayname_array["ktimetracker"] = "KTimeTracker";
$appdisplayname_array["trojita"] = "Trojita";
#extragear-sdk
$appdisplayname_array["xsldbg"] = "xsldbg";
$appdisplayname_array["kxsldbg"] = "KXSLDbg";
#extragear-sysadmin
$appdisplayname_array["partitionmanager"] = "Partition Manager";
$appdisplayname_array["kiosktool"] = "KIOSK Admin Tool";
#extragear-utils
$appdisplayname_array["katimon"] = "Katimon";
$appdisplayname_array["kdesrc-build"] = "kdesrc-build Script";
$appdisplayname_array["kdiff3"] = "KDiff3";
$appdisplayname_array["keurocalc"] = "KEuroCalc";
$appdisplayname_array["kpager"] = "KPager";
$appdisplayname_array["krecipes"] = "Krecipes";
$appdisplayname_array["kronometer"] = "Kronometer";
$appdisplayname_array["krusader"] = "Krusader";
$appdisplayname_array["nepomukshell"] = "Nepomuk Shell";
$appdisplayname_array["rsibreak"] = "RSIBreak";
#kdereview
$appdisplayname_array["kdots"] = "KDots";
$appdisplayname_array["kid3"] = "Kid3";
$appdisplayname_array["kpeg"] = "KPeg";
$appdisplayname_array["rkward"] = "RKWard";
$appdisplayname_array["rkwardplugins"] = "RKWard Plugins";
$package_no_application_array = array("kdepimlibs", "kdepim-runtime");
?>
