<?php
/*
	Produce lists later used to iterate over 
	the according level in the
	generated_used.inc.php array
	Generated variables:
		$searchbranchlist:	A list of all search branches
		$packagelist:	An list of all available packages
*/

$searchbranchlist = array( "trunk", "stable" );

/* 
	Weights of packages: helper function for cmp_packages()
*/
function package_weight( $p )
{
	$weights = array(
		'frameworks' => 0,
		'kdelibs' => 0,
		'calligra' => 5,
		'kdereview' => 10,
	);
	if ( array_key_exists($p, $weights) ) {
		$w = $weights[$p];
	} elseif ( strncmp($p, 'extragear-', strlen('extragear-')) == 0 ) {
		$w = 7;
	} else {
		$w = 2;
	}
	return $w;
}

/*
	Compare packages, so that a certain order is enforced
	(libraries first, then Applications, Extragear and Review)
*/
function cmp_packages( $p1, $p2 )
{
	$w1 = package_weight( $p1 );
	$w2 = package_weight( $p2 );
	$dw = $w1 - $w2;
	if ( $dw == 0 ) {
		/* same weight, so normal comparison */
		return strcmp( $p1, $p2 );
	} else {
		return $dw;
	}
}

/* $packagelist: Complete list of all available packages */
$packagelist = array_keys( $modules_programs );
usort( $packagelist, 'cmp_packages' );
/* Check if the requested package is a valid one */
if ( $selected_package != "" and
    (! in_array($selected_package, $packagelist) ) ) {
    $selected_package_nonexist = $selected_package;
    $show_intro = true;
}

?>